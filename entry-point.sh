#!/bin/bash

# Data directory
DB1_DATA_DIR="/var/lib/mongo1"
DB2_DATA_DIR="/var/lib/mongo2"
DB3_DATA_DIR="/var/lib/mongo3"

# Log directory
DB1_LOG_DIR="/var/log/mongodb1"
DB2_LOG_DIR="/var/log/mongodb2"
DB3_LOG_DIR="/var/log/mongodb3"

# Ports
DB1_PORT=27017
DB2_PORT=27018
DB3_PORT=27019

# Environment variables at runtime, or default values
HOST_NAME="${HOST:-localhost}"
REPLICA_SET_NAME="${REPLICA_SET:-rs0}"

echo "Using environment variables HOST_NAME: ${HOST_NAME}, REPLICA_SET_NAME: ${REPLICA_SET_NAME}"

mongod --dbpath ${DB1_DATA_DIR} --logpath ${DB1_LOG_DIR}/mongod.log --fork --port 27017 --bind_ip_all --replSet $REPLICA_SET_NAME
mongod --dbpath ${DB3_DATA_DIR} --logpath ${DB3_LOG_DIR}/mongod.log --fork --port 27019 --bind_ip_all --replSet $REPLICA_SET_NAME
mongod --dbpath ${DB2_DATA_DIR} --logpath ${DB2_LOG_DIR}/mongod.log --fork --port 27018 --bind_ip_all --replSet $REPLICA_SET_NAME

RS_MEMBER_1="{ \"_id\": 0, \"host\": \"${HOST_NAME}:${DB1_PORT}\", \"priority\": 2 }"
RS_MEMBER_2="{ \"_id\": 1, \"host\": \"${HOST_NAME}:${DB2_PORT}\", \"priority\": 0 }"
RS_MEMBER_3="{ \"_id\": 2, \"host\": \"${HOST_NAME}:${DB3_PORT}\", \"priority\": 0 }"

# mongosh --eval "rs.initiate({ \"_id\": \"${REPLICA_SET_NAME}\", \"members\": [${RS_MEMBER_1}, ${RS_MEMBER_2}, ${RS_MEMBER_3}] })"
mongosh --host ${HOST_NAME} --port ${DB1_PORT} --eval "rs.initiate({ \"_id\": \"${REPLICA_SET_NAME}\", \"members\": [${RS_MEMBER_1}, ${RS_MEMBER_2}, ${RS_MEMBER_3}] })"

echo "Replica set initiated"
echo "$(mongosh --eval "rs.status()")"

# Keep the container running
tail -f /dev/null