# mongo-rs

A Replica Set of MongoDB 6 running in a Docker container. 

**Not suitable for production**, the purpose is to have a *ready-made* replica set of MongoDB running inside docker container for CI tests and local development.

## How to run
Execute the following, where the environment variable `HOST` is optional to configure the replica's hostname in docker. By default, it uses `localhost`
There is also an optional `REPLICA_SET` variable which by default it uses `rs0`.

```shell
docker run \
    --name mongors \
    -e HOST=mongors \
    -p 27017:27017 \
    -p 27018:27018 \
    -p 27019:27019 \
    registry.gitlab.com/sunnyatticsoftware/sandbox/mongo-rs:latest
```

or, if you prefer, with docker-compose run

```shell
docker-compose up -d
```

When using `HOST` environment variable, make sure you edit your `etc/hosts` file with the desired hostname as an alias to localhost.

NOTE: It's possible to delay further than 30 seconds the initialization of replica set, to avoid errors. Set environment variable `DELAY_INITIATE` to whichever number.

```
# /etc/hosts
127.0.0.1 mongors
```
The connection string would be

```
mongodb://localhost:27017,localhost:27018,localhost:27019/?replicaSet=rs0&readPreference=primary&ssl=false
```

Replace `localhost` with the `HOST_NAME` environment variable's value, like this

```
mongodb://mongors:27017,mongors:27018,mongors:27019/?replicaSet=rs0&readPreference=primary&ssl=false
```

## Original project
This repository is inspired by [this GitHub repository](https://github.com/CandisIO/mongo-replica-set)